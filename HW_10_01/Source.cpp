#include <iostream>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <cmath>
using namespace std;
const int size_ = 5;
int arr[size_][size_];
int input_number = 0;
char input_cha;
int input() {	
	cout << "Please input number: ";
	cin >> input_number;
	cout << "\n";
	return input_number;
}

char input_ch() {
	do {
		cout << "Please input 'R' to shift right OR 'L' to shift left\n";
		cout << "Please input 'U' to shift up    OR 'D' to shift down\n";
		cin >> input_cha;
		cout << "\n";
	} while ((input_cha != 'R') && (input_cha != 'L') && (input_cha != 'U') && (input_cha != 'D'));
	return input_cha;
}

void arr_init() {
	for (int i = 0; i < size_; i++)
	{
		for (int j = 0; j < size_; j++)
			{
			arr[i][j] = rand()%100;
			}
	}
}


void arr_load() {
	for (int  i = 0; i < size_; i++)
	{
		int in_num = input();
		for (int  j = 0; j < size_; j++)
		{
			arr[i][j] = in_num * pow(2, j);
		}
	}
}
void arr_load_1() {
	for (int i = 0; i < size_; i++)
	{
		int in_num = input();
		for (int j = 0; j < size_; j++)
		{
			arr[i][j] = in_num + j;
		}
	}
}

void arr_print() {
	cout << "\n";
	for (int i = 0; i < size_; i++)
	{
		for (int j = 0; j < size_; j++)
		{
			cout << arr[i][j]<<"	";
		}
		cout << "\n";
	}
}

void arr_shift(int N, char side) {
	int temp = 0;
	for (int n = 0; n < N; n++)
	{
		switch (side)
		{
		case 'L':
			for (int i = 0; i < size_; i++)
		{	
			temp = arr[i][0];
			for (int j = 0; j < size_-1; j++)
			{
				arr[i][j] = arr[i][j + 1];
			}
			arr[i][size_-1] = temp;
		}
			break;
		case 'R':
			for (int i = 0; i < size_; i++)
		{
			temp = arr[i][size_-1];
			for (int j = size_-1; j > 0; j--)
			{
				arr[i][j] = arr[i][j - 1];
			}
			arr[i][0] = temp;
		}
			break;
		case 'U':
			for (int j = 0; j < size_; j++)
		{
			temp = arr[0][j];
			for (int i = 0; i < size_ - 1; i++)
			{
				arr[i][j] = arr[i+1][j];
			}
			arr[size_-1][j] = temp;
		}
			break;
		case 'D':
			for (int j = 0; j < size_; j++)
		{
			temp = arr[size_-1][j];
			for (int i = size_-1; i > 0; i--)
			{
				arr[i][j] = arr[i - 1][j];
			}
			arr[0][j] = temp;
		}
			break;
		default:
			break;
		}
	}
}

void task1() {
	arr_init();
	arr_load();
	arr_print();
	cout << endl << endl;
}

void task2() {
	arr_init();
	arr_load_1();
	arr_print();
	cout << endl << endl;
}

void task3() {
	arr_init();
	arr_print();
	arr_shift(input(), input_ch());
	arr_print();
}


int main()
{
	srand(time(NULL));
	int t;
	do
	{
		cout << "\n\nenter # task(to exit enter negative value):  ";
		cin >> t;
		switch (t)
		{
		case 1:
			task1();
			break;
		case 2:
			task2();
			break;
		case 3:
			task3();
			break;
		
		default:
			break;
		}
	} while (t >= 0);
	return 0;
}